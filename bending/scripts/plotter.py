import matplotlib.pyplot as plt
import makedir


# Function creates a plot of the data which has been passed to it and writes a .png image to the plots dirrectory.
def plotter(filename, extension, load):

	plt.plot(extension, load) #the two lists we added to are plotted here
        plt.title(filename)
        plt.xlabel('Extension (mm)')
        plt.ylabel('Load (N)')
	plt.savefig( "plots/{}_plot.png".format(filename)) #and output to a file
