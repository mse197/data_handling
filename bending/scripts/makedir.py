# Import the operating system module to allow os dependent functionality
#  Creating directories, opening files, etc

import os

dirname1 = 'plots'
dirname2 = 'xlxs'

if not os.path.exists(dirname1):  # determines is the folder exists
	os.makedirs(dirname1)     # creates the folder if it does not exist

if not os.path.exists(dirname2):  # determines is the folder exists
	os.makedirs(dirname2)     # creates the folder if it does not exist
