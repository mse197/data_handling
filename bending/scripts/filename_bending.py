import numpy

# Function to return the extension list
def get_extension(file_in):

        # Creates variable file and opens bending data file
        file = open(file_in)

        # Creates lists for the extension
        extension = []

        # Runs a loop to parse the extension data into the list.
        for line in file:
                a  = numpy.loadtxt(file_in,comments='"',delimiter=',')
                extension.append((a[1]))

        return extension


# Function to return the load list
def get_load(file_in):

        # Creates variable file and opens bending data file
        file = open(file_in)

        # Creates lists for the load
        load = []

        # Runs a loop to parse the load data into the list.
        for line in file:
                a  = numpy.loadtxt(file_in,comments='"',delimiter=',')
                load.append((a[2]))

	return load


