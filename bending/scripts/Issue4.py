import numpy

#numpy.loadtxt will load data from text file
#comments argument will ignore words starting with quotations
#delimiter argument will separate data that has commas

#data = numpy.loadtxt("Sp15_245L_sect-001_group-2-4_bendtest-steel.raw",comments='"',delimiter=',')

#Prints the last element in the array

#print data[-1]


# Allows the user to specify the file to analyze.
def create_name():
	filename = raw_input('Please select the file you want:')
	return filename

# Function to return the extension data in a list
def get_extension(file_in):
	
	# Creates variable file and opens bending data file
	file = open(file_in)
	
	# Creates lists for the extension
	extension = []

	a  = numpy.loadtxt(file_in,comments='"',delimiter=',')
        extension.append((a[1]))

	return a[:,1]


# Function to return the load data in a list
def get_load(file_in):
        
        # Creates variable file and opens bending data file
        file = open(file_in)
        
        # Creates lists for the load
        load = []

        # Runs a loop to parse the load data into the list.
        a  = numpy.loadtxt(file_in,comments='"',delimiter=',')
        load.append((a[2]))
	
	return a[:,2]
