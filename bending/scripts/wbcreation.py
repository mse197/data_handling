from openpyxl import Workbook
import filename_bending

# Created new function 'create_sheet' and passes in name of imported file and element name.
def create_sheet(material_in, extension, laod):

        # Creates a workbook and worksheet within the workbook.
        wb = Workbook()
        ws = wb.active

        # Creates headers for the columns in .xls sheet
        ws["A1"] = "Extension"
        ws["B1"] = "Load"


        # Uses loop to write the arrays wavelength and intensity into .xls spreadsheet
        # in range: 2 starts at row 2 in the worksheet and +2 accounts for offset created.
        # Offset of 2 is created by the column header and the lack of a row 0 in the spreadsheet.
        for num in range(2,len(wavelength)+2):
                ws["A{row}".format(row=num)] = extension[num-2]
                ws["B{row}".format(row=num)] = load[num-2]

        # Closes the workbook and saves the file with the element name.
        wb.save("{}.xlsx".format(material_in))

