import Issue4
import makedir
from openpyxl import Workbook
import sys
import string
import os
import plotter

# The file names vary more for the bend tests and the solution from the spectra program won't work as well

# Creates and prints the list of bend test files with the .raw extension for the user to choose from
wb = Workbook()
ws = wb.active

def get_files():
	items = os.listdir(".")
	files = []
	for name in items:
		if name.endswith(".raw"):
			files.append(name)

	return files

items = os.listdir(".")
files = []
for name in items:
	if name.endswith(".raw"):
		files.append(name)

for i in range(len(files)):
	if i == 0:	
		print '\nThe available files are:\n'
	print i,"\t", files[i] 

print "\n"

# Prompt user to pick a filename using the index that was printed 
#   This is to save the user from having to type the full filename
#      userinfo is the integer corresponding to the filename

userinfo = input("select the number that corresponds to the file you wish to plot: ")

filepath = files[int(userinfo)]

load = Issue4.get_load(filepath)
extension = Issue4.get_extension(filepath)

plotter.plotter(filepath, extension, load)

ws["A1"] = "Load (N)"
ws["B1"] = "Extension (mm)"

for num in range(2,len(load)+2):
	ws["A{row}".format(row = num)] = load[num-2]
	ws["B{row}".format(row=num)] = extension[num-2]
wb.save("excel/{}.xlsx".format(filepath))

print "\n --> Your plot has been saved to:\n\n\t --> '%s\plots'\n\n" % os.getcwd() 
