## Goal:
The students in MSE 245L need your help!  Data from two different experiments is output in “raw” text files, and it is taking students and professors a lot of time and energy to convert them and analyze them.  Our task is to write programs that can automate these tasks.  This project has two parts spectra and bending, which have different file formats from two different experiments. 

## Info:
To run the code for all the spectra data do:

```
#!bash
$ bash loopall.sh
```

To run the code for bendplot do:
```
#!bash
$ python bending.py # You will be prompted to pick a filename.
```


## Results:
Commit f12ed70 is being used as the "final" project status.  

As of final status, the code in the 'spectra' project successfully reads in data 
in the 'data' folder and outputs excel spreadsheets and plots (in png format) in
two output directories. The plots are informative and the Excel spreadsheet can
be used by others.  -  Full Credit.

The code in the 'bending' project looks for data files and gives the user a choice
of which file to parse and plot.  The plots are informative and the spreadsheets
output look great (and show that sometimes users run multiple tests per spreadsheet).
This code will need some minor tweaks to let a user specify a particular file to parse.
 - Full Credit.
