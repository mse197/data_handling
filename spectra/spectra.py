import sys
sys.path.append('./scripts')	#adds the ./scripts path so spectra.py imports our scripts
import filename
import Plotter
import os
import wbcreation

# Stores the system input into the variable into element which can be passed into other functions.
element = sys.argv[1]

#print spectra_file using import filename, assigned to variables our_file,element
our_file, element = filename.create_name(element)

# Uses functions within filename.py to create wavelenght and intensity as global varaibles.  These can be passed into wbcreation and Plotter functions.
wavelength = filename.get_wavelength(our_file)
intensity = filename.get_intensity(our_file)

# Calls create_sheet function to write .xls spreadsheet using the imported file and element name.
wbcreation.create_sheet(element, wavelength, intensity)

Plotter.plotter(element, wavelength, intensity)

# Outputs where the plot and workbook have been saved
# New line commands and tab added to provide easier reading of output information
print"\n --> Program complete, check directory:\n\n\t --> '%s' to locate graphs and workbook data\n\n" % os.getcwd()

