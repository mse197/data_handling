
# Returns file name and element from system input passed into function
def create_name(element):
	prefix='data/Sp15_245L_sect-001_group-2-4_spectrum-'
	f = prefix + element
	return f, element

# Function to return the wavelength list
def get_wavelength(file_in):
	
	# Creates variable file and opens spectra data file
	file = open(file_in)
	
	# Creates lists for the wavelength
	wavelength = []

	# Runs a loop to parse the wavelenght data into the list.
	for line in file:
		a = line.split() #this splits a line based on the whitespace, into multiple words
            	wavelength.append(float(a[0]))

	return wavelength


# Function to retrun the intensity list
def get_intensity(file_in):
        
        # Creates variable file and opens spectra data file
        file = open(file_in)
        
        # Creates lists for the wavelength
        intensity = []

        # Runs a loop to parse the wavelenght data into the list.
        for line in file:
                a = line.split() #this splits a line based on the whitespace, into multiple words
                intensity.append(float(a[1]))

	return intensity
