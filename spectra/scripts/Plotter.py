import matplotlib.pyplot as plt

# Function creates a plot of the data which has been passed to it and writes a .png image to the plots directory.
def plotter( element, wavelength, intensity):

	plt.plot(wavelength,intensity) #the two lists we added to are plotted here
        plt.title('Wavelength as a Function of Intensity')
        plt.xlabel('Wavelength (m)')
        plt.ylabel('Intensity (W/m^2)')
	plt.savefig( "plots/{}.png".format(element )) #and output to a file

