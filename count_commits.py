import subprocess
alias = {'majeed':['Majeed'],
         'monet':['Monet'],
         'ziyad':['Ziyad','ziyad'],
         'andres':['Andres'],
         'jon':['jonhuff'],
         'steve':['stevejohns'],
         'beatriz':['Beatriz'],
         'donald':['Donald'],
         'leticia':['Leticia','leticialeite'],
         'ryan':['Ryan'],
         'jordan':['Jordan'],
         'heather':['heather'],
         'eric':['Eric']
        }
commits = {}
for s in alias.keys():
    commits[s] = 0
day_1 = 19
day_2 = 26
teams_1 = [['majeed','ziyad'],['monet','leticia','beatriz'],['steve','andres'],['heather','jordan'],['jon','ryan','donald']]
teams_2 = [['majeed','ziyad'],['monet','leticia','beatriz'],['steve','ryan'],['heather','jordan'],['jon','donald']]

def get_day(line):
    words = line.split()
    return int(words[-3].split('-')[2])

def get_name(line):
    words = line.split()
    for k,v in alias.iteritems():
        if words[0] in v:
            return k
    print "Error: Name not found!"
    exit()

def get_team(day,name):
    if day==day_1:
        for i in teams_1:
            if name in i:
                return i
    if day==day_2:
        for i in teams_2:
            if name in i:
                return i
    return None

def find_team_commits():
    cmd = 'git log --oneline --cherry-pick --pretty="%an %ci" --branches=* '
    a = subprocess.check_output(cmd,shell=True)
    lines = a.split('\n')
    for l in lines:
        if len(l) > 0:
            day = get_day(l)
            name = get_name(l)
            team = get_team(day,name)
            if team is not None:
                for m in team:
                    commits[m]+=1
            else:
                commits[name]+=1

def calc_grade():
    for k,v in commits.iteritems():
        points = v*3
        if points > 45:
            points = 45
        print "{}\t{}\t{}/45".format(k,v,points)

find_team_commits()
calc_grade()


